from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gestion.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'digicode.views.home', name='home'),
    url(r'^login/$', 'gestion.views.login_view', name='login'),
    url(r'^logout/$', 'gestion.views.logout_view', name='logout'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^rpc/', include('rpc_server.urls')),

    url(r'^liste/edit_code/(?P<id>\d+)$','digicode.views.edit_code', name='edit_code'),
    url(r'^liste/add_code$','digicode.views.add_code', name='add_code'),
    url(r'^liste/$', 'digicode.views.liste', name='liste'),
    url(r'^local/([0-9]*)$', 'digicode.views.liste_respo', name='liste_respo'),
    url(r'^local/([0-9]*)/add_acces', 'digicode.views.add_acces', name='add_acces'),

    url(r'^calendar/(?P<month>[^/]*)/(?P<year>[^/]*)/(?P<local>[^/]*)/$', 'digicode.views.calendar', name='calendar'),
    url(r'^calendar/(?P<local>[^/]*)/$', 'digicode.views.calendar', name='calendar_now'),
    url(r'^calendar$', 'digicode.views.calendar', name='calendar_all'),
)

