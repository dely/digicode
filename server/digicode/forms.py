# -*- coding: utf-8 -*-

from django import forms
from models import Acces, Code, Local
import datetime

class AccesPonctuel(forms.ModelForm):
    class Meta:
        model = Acces
        fields = ['user', 'start_time', 'end_time']


class GenCodeForm(forms.ModelForm):
    """
        Formulaire permettant de générer un digicode pour les locaux
        accessible à l'user.

        Effectue les vérifications sur la disponibilité du code et des
        locaux.
    """

    def clean_touches(self):
        """
            Permet de vérifier que le code demandé n'est pas déjà pris.
        """
        code = self.cleaned_data['touches']

        if code in [ c.touches for c in Code.objects.all() ]:
            # Le code est déjà pris, on le signale à l'utilisateur

            raise forms.ValidationError('Ce code est déjà pris !')

        return code

    def clean_locaux(self):
        """
            Permet des vérifier que les locaux sont bien accessibles au
            propriétaire.
        """
        # Tout ce dont on a besoin pour la vérification
        locaux = self.cleaned_data['locaux']
        proprio = self.cleaned_data['proprietaire']

        now = datetime.datetime.now()

        # On fabrique la liste des locaux dispo sans doublons ( d'où le list(set(list())) )
        available_ones = list(set([ a.local for a in Acces.objects.filter(user=proprio,end_time__gte=now) ]))
        forbidden_ones = list()

        # On établit la liste des locaux indisponibles
        for local in locaux:
            if local not in available_ones:
                forbidden_ones.append(local.nom)

        if forbidden_ones:
            raise forms.ValidationError('Ces locaux ne sont pas disponibles : ' + ','.join(forbidden_ones))

        return locaux



    class Meta:
        verbose_name = 'code'
        model = Code
        fields = ['proprietaire','touches','locaux',]
        widgets = {
            'locaux' : forms.widgets.CheckboxSelectMultiple,
            'proprietaire' : forms.widgets.HiddenInput,
        }
