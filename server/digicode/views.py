# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.http import HttpResponseForbidden
from django.core.exceptions import SuspiciousOperation
import calendar as calendar_module
import datetime
from models import Code, Local, Acces
from forms import AccesPonctuel,GenCodeForm

def home(request):
    return render(request, "home.html", {'liste_local' : Local.objects.all()})

@login_required
def liste(request):
    """
        Affiche la liste des codes disponibles pour un utilisateur donné,
        ainsi que les locaux associés.
    """
    code_list = list()

    # On contruit la liste des codes et les locaux associés
    for c in Code.objects.filter(proprietaire=request.user):
        code_list.append((c,c.locaux.all()))

    return render(request, "liste.html", {
        'liste': code_list,
    })

@login_required
def liste_respo(request, pk):
    #
    local = get_object_or_404(Local, pk=pk)
    #
    accesses = Acces.objects.all().filter(local=local)
    mine = accesses.filter(user=request.user)
    responsable = bool(mine.filter(responsable=True))
    if not mine:
        return HttpResponseForbidden("forbidden")

    return render(request, "liste_respo.html", {
        'local': local,
        'accesses': accesses,
        'responsable': responsable,
    })

@login_required
def edit_access(request, pk):
    acc = get_object_or_404(Acces, pk=pk)
    # Ai-je le droit ?
    if not Acces.objects.all().filter(local=local, user=request.user):
        return HttpResponseForbidden("forbidden")
    return

@login_required
def add_acces(request, pk):
    local = get_object_or_404(Local, pk=pk)
    # Ai-je le droit ?
    if not Acces.objects.all().filter(local=local, user=request.user):
        return HttpResponseForbidden("forbidden")
    if request.method == 'POST':
        form = AccesPonctuel(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.local = local
            obj.responsable = False
            obj.save()
            # TODO: message de succès
            return redirect('liste_respo', pk)
    else:
        form = AccesPonctuel()
    return render(request, 'add_acces.html', {
        'form': form,
        'local': local,
    })


@login_required
def add_code(request):
    """
        Vue permettant à l'utilisateur de générer un nouveau code en fonction
        de ses accès. La disponibilité du code et des locaux est vérifié par
        le GenCodeForm au moment de la validation.
    """
    user = request.user

    new_code = Code(proprietaire=user)

    if request.method == 'POST':
        # Soumission de formulaire
        f = GenCodeForm(request.POST,instance=new_code)

        if f.is_valid():
            # On vérifie que proprietaire est bien identique à user
            if f.cleaned_data['proprietaire'].id != user.id:
                raise SuspiciousOperation()

            f.save()

            return redirect('liste')

        else:
            return render(request,"add_code.html",{
                                                    'form' : f ,
                                                  })
    else:
        # Envoi d'un formulaire
        f = GenCodeForm(instance=new_code)

        return render(request,"add_code.html",{
                                                  'form' : f,
                                              })


@login_required
def edit_code(request,id):
    """
        Vue permettant la modification ou la suppression d'un code.
        La disponibilité du code et des locaux est vérifié par le GenCodeForm
        au moment de la validation.
    """

    user = request.user

    # On vérifie dans un premier temps que le code appartient bien à l'user
    code = Code.objects.get(id=id)

    if code.proprietaire.pk != user.pk:
        # Il ne s'agit pas d'un code de l'user
        # On renvoie une 403
        return HttpResponseForbidden("Forbidden")

    # Là on est sur qu'il s'agit du bon proprio
    if request.method == 'POST':
        # Il s'agit d'une modification
        # On récupère les données
        f = GenCodeForm(request.POST,instance=code)
        if f.is_valid():
            f.save()

            code_list = list()

            # On contruit la liste des codes et les locaux associés
            for c in Code.objects.filter(proprietaire=request.user):
                        code_list.append((c,c.locaux.all()))

            return render(request, "liste.html", { 'liste': code_list })


        return render(request,"add_code.html",{
                                                  'form'   : f ,
                                              })
    else:
        action = request.GET['action']
        if action == 'delete':
            # On supprime le code
            code.delete()

            code_list = list()

            # On contruit la liste des codes et les locaux associés
            for c in Code.objects.filter(proprietaire=request.user):
                code_list.append((c,c.locaux.all()))

            return render(request,"liste.html",{ 'liste' : code_list })

        elif action == 'edit':
            # On édite le code
            f = GenCodeForm(instance = code)

            return render(request,"add_code.html",{
                                                      'form' : f,
                                                  })
        else:
            raise NotImplementedError



@login_required
def calendar(request, month=None, year=None, local=None):
    today = datetime.date.today()
    month = month or today.month
    year = year or today.year
    if not month and not year:
        return HttpResponseForbidden()
    c = calendar_module.Calendar()
    calendr = c.monthdatescalendar(int(year),int(month))
    if not local :
        local = Local.objects.all()[0]
    month_start = calendr[0][0]
    month_end = calendr[len(calendr)-1][6]
    reservations = Acces.objects.all().filter(start_time__lte = month_end,end_time__gte = month_start).order_by('start_time')
    final_calendar=[]
    for i in xrange(len(calendr)):
        final_calendar.append([])
        for j in xrange(7):
            final_calendar[i].append([calendr[i][j]])
            for reservation in reservations:
                 if reservation.start_time.date() <= calendr[i][j] <= reservation.end_time.date():
                     final_calendar[i][j].append(reservation)
    return render(request, "calendar.html", {'calendar' : final_calendar})


